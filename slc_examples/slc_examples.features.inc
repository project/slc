<?php
/**
 * @file
 * slc_examples.features.inc
 */

/**
 * Implements hook_views_api().
 */
function slc_examples_views_api() {
  return array("version" => "3.0");
}
